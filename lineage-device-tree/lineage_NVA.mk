#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from NVA device
$(call inherit-product, device/hmd/NVA/device.mk)

PRODUCT_DEVICE := NVA
PRODUCT_NAME := lineage_NVA
PRODUCT_BRAND := Nokia
PRODUCT_MODEL := Nokia C12
PRODUCT_MANUFACTURER := hmd

PRODUCT_GMS_CLIENTID_BASE := android-hmd

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="Nova_00WW-user 12 SP1A.210812.016 V1.540_B01 release-keys"

BUILD_FINGERPRINT := Nokia/Nova_00WW/NVA:12/SP1A.210812.016/00WW_1_540:user/release-keys
